#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofBackground(80);
    ofSetFrameRate(60);
    ofSetVerticalSync(true);
    w = 640;
    h = 480;
    
    cam.initGrabber(w, h, OF_IMAGE_COLOR);
    
    legoSource.load("lego.png");
    screenRatio = 1024.0 / cam.getWidth(); //2
    
    legoSize = 10;
    legoImagedWidth = w / legoSize;  // 64
    legoImageHeight = h / legoSize;  // 48
    
    int totPixels = legoImagedWidth * legoImageHeight;
    pixelColorR.assign(totPixels,0);
    pixelColorG.assign(totPixels,0);
    pixelColorB.assign(totPixels,0);
}

//--------------------------------------------------------------
void ofApp::update()
{
    cam.update();
    
    if(cam.isFrameNew())
    {
        ofPixels pix = cam.getPixels();
        unsigned char * pixels = pix.getPixels();
        
        float R = 0;
        float G = 0;
        float B = 0;
        
        pixelColorR.clear();
        pixelColorG.clear();
        pixelColorB.clear();
        
        for (int i = 0; i < legoImageHeight; i++) // 48
        {
            for (int j = 0; j < legoImagedWidth; j++) // 64
            {
                // per ogni pixels mi indicizzo in modo lineare i pixels per poterli poi inserire nel vettore
                int index = i * w + j;
                
                // mi prendo tutti i pixels proveniente dalla camera considerando il gap di legoSize
                int legoPixelIndex =  index * 3 * legoSize;
                
                // i pixels R G B sono disposti in modo sequenziale nell'array *pixels perciò shifto di +0 +1 +2 perché per ogni pixels ho 3 componenti (rgb)
                R = pixels[legoPixelIndex + 0];
                G = pixels[legoPixelIndex + 1];
                B = pixels[legoPixelIndex + 2];
                 
                int grey = (R + G + B) / 3;
                
                // salvo i colori nei vettori float e li aggiorno ad ogni frame
                pixelColorR.push_back(R);
                pixelColorG.push_back(G);
                pixelColorB.push_back(B);
            }
        }
        
    }
    
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetWindowTitle("LEGO BLOCK " + ofToString(ofGetFrameRate(),0));
    
    // controllo se i vettori non sono vuoti
    if (pixelColorR.size() != 0 && pixelColorG.size() && pixelColorB.size())
    {
        for (int i = 0; i < legoImageHeight; i++)
        {
            for (int j = 0; j < legoImagedWidth; j++)
            {
                int index = j * legoImagedWidth + i;

                ofPushStyle();
                ofSetColor( ofColor(pixelColorR[index], pixelColorG[index], pixelColorB[index], 255) );

                float X = i * legoSize * screenRatio;
                float Y = j * legoSize * screenRatio;
                float SIZE = legoSize * screenRatio;
                
                legoSource.draw(X, Y, SIZE, SIZE);
                
                ofPopStyle();
            }
        }
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if(key == ' ')
    {
        cout << "pix cam : " << cam.getWidth() << " " << cam.getHeight() << endl;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
